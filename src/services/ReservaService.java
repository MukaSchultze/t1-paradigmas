package services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import models.Reserva;

public class ReservaService extends SQLService<Reserva, Integer> {

    public ReservaService() {
        setSqlCreate("INSERT INTO reservas (isbn, login, retirada) VALUES (?, ?, ?)");
        setSqlUpdate("UPDATE reservas SET login = ?, retirada = ? WHERE isbn = ?");
        setSqlDelete("DELETE FROM reservas WHERE isbn = ?");
        setSqlFetch("SELECT * FROM reservas WHERE isbn = ?");
        setSqlFetchAll("SELECT * FROM reservas ORDER BY retirada ASC");
    }

    @Override
    protected Reserva createObjFromResult(ResultSet rs) throws SQLException {
        Reserva reserva = new Reserva();

        reserva.setIsbn(rs.getInt("isbn"));
        reserva.setUsuario(rs.getString("login"));
        reserva.setRetirada(rs.getTimestamp("retirada").toLocalDateTime());

        return reserva;
    }

    @Override
    protected void fillCreateStatement(PreparedStatement ps, Reserva obj) throws SQLException {
        ps.setInt(1, obj.getIsbn());
        ps.setString(2, obj.getUsuario().getLogin());
        setTimestamp(ps, 3, obj.getRetirada());
    }

    @Override
    protected void fillUpdateStatement(PreparedStatement ps, Reserva obj) throws SQLException {
        ps.setString(1, obj.getUsuario().getLogin());
        setTimestamp(ps, 2, obj.getRetirada());
        ps.setInt(3, obj.getIsbn());
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement ps, Reserva obj) throws SQLException {
        ps.setInt(1, obj.getIsbn());
    }

    @Override
    protected void fillFetchStatement(PreparedStatement ps, Integer key) throws SQLException {
        ps.setInt(1, key);
    }

}