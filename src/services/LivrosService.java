package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import models.Livro;

public class LivrosService extends SQLService<Livro, Integer> {

    private String sqlFetchTitulo;
    private String sqlFetchEditora;

    public LivrosService() {
        setSqlCreate("INSERT INTO livros (isbn, titulo, editora, autores, edicao, ano) VALUES (?, ?, ?, ?, ?, ?)");
        setSqlUpdate("UPDATE livros SET titulo = ?, editora = ?, autores = ?, edicao = ?, ano = ? WHERE isbn = ?");
        setSqlDelete("DELETE FROM livros WHERE isbn = ?");
        setSqlFetch("SELECT * FROM livros WHERE isbn = ?");
        setSqlFetchAll("SELECT * FROM livros ORDER BY titulo ASC");

        this.sqlFetchTitulo = "SELECT * FROM livros WHERE titulo LIKE ? ORDER BY titulo ASC";
        this.sqlFetchEditora = "SELECT * FROM livros WHERE editora LIKE ? ORDER BY titulo ASC";

    }

    public Collection<Livro> fetchTitulo(String titulo) {
        try {
            Connection connection = openConnection();
            PreparedStatement ps = connection.prepareStatement(sqlFetchTitulo);
            ps.setString(1, "%" + titulo + "%");
            ResultSet rs = ps.executeQuery();
            Collection<Livro> result = createCollectionFromResult(rs);
            rs.close();
            ps.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection<Livro> fetchEditora(String editora) {
        try {
            Connection connection = openConnection();
            PreparedStatement ps = connection.prepareStatement(sqlFetchEditora);
            ps.setString(1, "%" + editora + "%");
            ResultSet rs = ps.executeQuery();
            Collection<Livro> result = createCollectionFromResult(rs);
            rs.close();
            ps.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected Livro createObjFromResult(ResultSet rs) throws SQLException {
        Livro livro = new Livro();

        livro.setISBN(rs.getInt("isbn"));
        livro.setTitulo(rs.getString("titulo"));
        livro.setEditora(rs.getString("editora"));
        livro.setAutores(rs.getString("autores"));
        livro.setEdicao(rs.getString("edicao"));
        livro.setAno(rs.getInt("ano"));

        return livro;
    }

    @Override
    protected void fillCreateStatement(PreparedStatement ps, Livro obj) throws SQLException {
        ps.setInt(1, obj.getISBN());
        ps.setString(2, obj.getTitulo());
        ps.setString(3, obj.getEditora());
        ps.setString(4, obj.getAutores());
        ps.setString(5, obj.getEdicao());
        ps.setInt(6, obj.getAno());
    }

    @Override
    protected void fillUpdateStatement(PreparedStatement ps, Livro obj) throws SQLException {
        ps.setString(1, obj.getTitulo());
        ps.setString(2, obj.getEditora());
        ps.setString(3, obj.getAutores());
        ps.setString(4, obj.getEdicao());
        ps.setInt(5, obj.getAno());
        ps.setInt(6, obj.getISBN());
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement ps, Livro obj) throws SQLException {
        ps.setInt(1, obj.getISBN());
    }

    @Override
    protected void fillFetchStatement(PreparedStatement ps, Integer key) throws SQLException {
        ps.setInt(1, key);
    }

}