package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Collection;

import models.DataSet;
import program.Constants;

public abstract class SQLService<T extends DataSet, TKey> {

    private String sqlCreate;
    private String sqlUpdate;
    private String sqlDelete;
    private String sqlFetch;
    private String sqlFetchAll;

    // region Properties
    public String getSqlCreate() {
        return this.sqlCreate;
    }

    public void setSqlCreate(String value) {
        this.sqlCreate = value;
    }

    public String getSqlUpdate() {
        return this.sqlUpdate;
    }

    public void setSqlUpdate(String value) {
        this.sqlUpdate = value;
    }

    public String getSqlDelete() {
        return this.sqlDelete;
    }

    public void setSqlDelete(String value) {
        this.sqlDelete = value;
    }

    public String getSqlFetch() {
        return this.sqlFetch;
    }

    public void setSqlFetch(String value) {
        this.sqlFetch = value;
    }

    public String getSqlFetchAll() {
        return this.sqlFetchAll;
    }

    public void setSqlFetchAll(String value) {
        this.sqlFetchAll = value;
    }
    // endregion

    public static Connection openConnection() {
        Connection c = null;

        try {
            Class.forName(Constants.SQL_JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("Driver não encontrado " + Constants.SQL_JDBC_DRIVER);
        }

        try {
            c = DriverManager.getConnection(Constants.SQL_URL_CONEXAO, Constants.SQL_USER, Constants.SQL_PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;

    }

    // region CRUD
    public void create(T obj) throws SQLException {
        Connection connection = openConnection();
        PreparedStatement ps = connection.prepareStatement(getSqlCreate());
        fillCreateStatement(ps, obj);
        ps.execute();
        ps.close();
        connection.close();
    }

    public void update(T obj) throws SQLException {
        Connection connection = openConnection();
        PreparedStatement ps = connection.prepareStatement(getSqlUpdate());
        fillUpdateStatement(ps, obj);
        ps.execute();
        ps.close();
        connection.close();
    }

    public boolean delete(T obj) {
        try {
            Connection connection = openConnection();
            PreparedStatement ps = connection.prepareStatement(getSqlDelete());
            fillDeleteStatement(ps, obj);
            ps.execute();
            ps.close();
            connection.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public T fetch(TKey key) {
        try {
            T result = null;
            Connection connection = openConnection();
            PreparedStatement ps = connection.prepareStatement(getSqlFetch());
            fillFetchStatement(ps, key);
            ResultSet rs = ps.executeQuery();

            if (rs.next())
                result = createObjFromResult(rs);

            rs.close();
            ps.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection<T> fetchAll() {
        try {
            Connection connection = openConnection();
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(getSqlFetchAll());
            Collection<T> result = createCollectionFromResult(rs);
            rs.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    // endregion

    protected void setTimestamp(PreparedStatement ps, int column, LocalDateTime timestamp) throws SQLException {
        Timestamp ts = Timestamp.from(timestamp.toInstant(ZoneOffset.UTC));
        ps.setTimestamp(column, ts);
    }

    // region Abstract
    protected abstract T createObjFromResult(ResultSet rs) throws SQLException;

    protected Collection<T> createCollectionFromResult(ResultSet rs) throws SQLException {
        Collection<T> array = new ArrayList<T>();
        while (rs.next()) {
            T temp = createObjFromResult(rs);
            array.add(temp);
        }
        return array;
    }

    protected abstract void fillCreateStatement(PreparedStatement ps, T obj) throws SQLException;

    protected abstract void fillUpdateStatement(PreparedStatement ps, T obj) throws SQLException;

    protected abstract void fillDeleteStatement(PreparedStatement ps, T obj) throws SQLException;

    protected abstract void fillFetchStatement(PreparedStatement ps, TKey key) throws SQLException;
    // endregion

}