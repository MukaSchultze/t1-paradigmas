package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import models.Emprestimo;
import models.Livro;

public class EmprestimoService extends SQLService<Emprestimo, Integer> {

    private String sqlFetchLogin;

    public EmprestimoService() {
        setSqlCreate("INSERT INTO emprestimos (isbn, login, retirado, entrega, renovacoes) VALUES (?, ?, ?, ?, ?)");
        setSqlUpdate("UPDATE emprestimos SET login = ?, retirado = ?, entrega = ?, renovacoes = ? WHERE isbn = ?");
        setSqlDelete("DELETE FROM emprestimos WHERE isbn = ?");
        setSqlFetch("SELECT * FROM emprestimos WHERE isbn = ?");
        setSqlFetchAll("SELECT * FROM emprestimos ORDER BY entrega ASC");

        this.sqlFetchLogin = "SELECT * FROM emprestimos WHERE login = ? ORDER BY isbn ASC";
    }

    public Collection<Emprestimo> fetchLogin(String login) {
        try {
            Connection connection = openConnection();
            PreparedStatement ps = connection.prepareStatement(sqlFetchLogin);
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            Collection<Emprestimo> result = createCollectionFromResult(rs);
            rs.close();
            ps.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection<Emprestimo> fetchTitulo(String titulo) {
        // Isso seria muito errado em uma situação real
        Collection<Livro> livros = new LivrosService().fetchTitulo(titulo);
        ArrayList<Emprestimo> emprestimos = new ArrayList<Emprestimo>();

        for (Livro livro : livros) {
            emprestimos.add(fetch(livro.getISBN()));
        }

        return emprestimos;
    }

    @Override
    protected Emprestimo createObjFromResult(ResultSet rs) throws SQLException {
        Emprestimo empr = new Emprestimo();

        empr.setIsbn(rs.getInt("isbn"));
        empr.setUsuario(rs.getString("login"));
        empr.setRetirado(rs.getTimestamp("retirado").toLocalDateTime());
        empr.setEntrega(rs.getTimestamp("entrega").toLocalDateTime());
        empr.setRenovacoes(rs.getInt("renovacoes"));

        return empr;
    }

    @Override
    protected void fillCreateStatement(PreparedStatement ps, Emprestimo obj) throws SQLException {
        ps.setInt(1, obj.getIsbn());
        ps.setString(2, obj.getUsuario().getLogin());
        setTimestamp(ps, 3, obj.getRetirado());
        setTimestamp(ps, 4, obj.getEntrega());
        ps.setInt(5, obj.getRenovacoes());
    }

    @Override
    protected void fillUpdateStatement(PreparedStatement ps, Emprestimo obj) throws SQLException {
        ps.setString(1, obj.getUsuario().getLogin());
        setTimestamp(ps, 2, obj.getRetirado());
        setTimestamp(ps, 3, obj.getEntrega());
        ps.setInt(4, obj.getRenovacoes());
        ps.setInt(5, obj.getIsbn());
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement ps, Emprestimo obj) throws SQLException {
        ps.setInt(1, obj.getIsbn());
    }

    @Override
    protected void fillFetchStatement(PreparedStatement ps, Integer key) throws SQLException {
        ps.setInt(1, key);
    }

}