package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import models.User;

public class UserService extends SQLService<User, String> {

    private String sqlFetchNome;

    public UserService() {
        setSqlCreate("INSERT INTO users (login, nome, senha, multa, tipo) VALUES (?, ?, ?, ?, ?)");
        setSqlUpdate("UPDATE users SET nome = ?, senha = ?, multa = ?, tipo = ? WHERE login = ?");
        setSqlDelete("DELETE FROM users WHERE login = ?");
        setSqlFetch("SELECT * FROM users WHERE login = ?");
        setSqlFetchAll("SELECT * FROM users ORDER BY nome ASC");

        this.sqlFetchNome = "SELECT * FROM users WHERE nome LIKE ? ORDER BY nome ASC";
    }

    public Collection<User> fetchNome(String nome) {
        try {
            Connection connection = openConnection();
            PreparedStatement ps = connection.prepareStatement(sqlFetchNome);
            ps.setString(1, "%" + nome + "%");
            ResultSet rs = ps.executeQuery();
            Collection<User> result = createCollectionFromResult(rs);
            rs.close();
            ps.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected User createObjFromResult(ResultSet rs) throws SQLException {
        User user = new User();

        user.setLogin(rs.getString("login"));
        user.setNome(rs.getString("nome"));
        user.setSenha(rs.getString("senha"));
        user.setMulta(rs.getFloat("multa"));
        user.setIsProfessor(rs.getInt("tipo") == 1);

        return user;
    }

    @Override
    protected void fillCreateStatement(PreparedStatement ps, User obj) throws SQLException {
        ps.setString(1, obj.getLogin());
        ps.setString(2, obj.getNome());
        ps.setString(3, obj.getSenha());
        ps.setFloat(4, obj.getMulta());
        ps.setInt(5, obj.getIsProfessor() ? 1 : 0);
    }

    @Override
    protected void fillUpdateStatement(PreparedStatement ps, User obj) throws SQLException {
        ps.setString(1, obj.getNome());
        ps.setString(2, obj.getSenha());
        ps.setFloat(3, obj.getMulta());
        ps.setInt(4, obj.getIsProfessor() ? 1 : 0);
        ps.setString(5, obj.getLogin());
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement ps, User obj) throws SQLException {
        ps.setString(1, obj.getLogin());
    }

    @Override
    protected void fillFetchStatement(PreparedStatement ps, String key) throws SQLException {
        ps.setString(1, key);
    }

}