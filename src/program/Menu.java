package program;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class Menu {

    protected Menu parent;

    protected abstract String getTitulo();

    protected abstract void draw();

    public void open(Menu parent) {
        this.parent = parent;
        System.out.println("");
        System.out.println(getTitulo());
        System.out.println("");
        draw();
    }

    protected void openOptions(Menu parent, Menu... menus) {

        String[] titles = new String[menus.length];

        for (int i = 0; i < menus.length; i++)
            titles[i] = menus[i].getTitulo();

        int selected = openOptions(titles);

        if (selected != menus.length)
            menus[selected].open(this);
        else if (parent != null)
            parent.open(parent.parent);

    }

    protected int openOptions(String... menus) {

        int i = 0;

        for (i = 0; i < menus.length; i++)
            System.out.println(i + " - " + menus[i]);

        System.out.println(i + " - Voltar");

        int read = IO.readInt("");

        if (read < 0 || read > menus.length) {
            System.out.println("Opção inválida");
            return openOptions(menus);
        }

        return read;

    }

    public static String niceDate(LocalDateTime timestamp) {
        return timestamp.format(DateTimeFormatter.ofPattern("HH:mm' de 'dd/MM/yyyy"));
    }

}