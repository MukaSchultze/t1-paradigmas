package program;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import models.User;
import services.UserService;

public class UserMenu extends Menu {

    @Override
    protected String getTitulo() {
        return "Usuários";
    }

    @Override
    protected void draw() {
        switch (openOptions("Novo usuário", "Deletar usuário", "Listar", "Buscar", "Pagar multa")) {
        case 0:
            novo();
            break;
        case 1:
            deletar();
            break;
        case 2:
            listar();
            break;
        case 3:
            buscar();
            break;
        case 4:
            pagarMulta();
            break;
        default:
            parent.open(parent.parent);
            return;
        }

        open(parent);
    }

    public void novo() {
        String login = IO.readString("Login: ");
        User user = new UserService().fetch(login);

        if (user != null) {
            System.out.println("Usuário já registrado");
            return;
        }

        user = new User();
        user.setLogin(login);
        user.setNome(IO.readString("Nome: "));
        user.setSenha(IO.readString("Senha: "));
        user.setIsProfessor(IO.readBool("Profesor (S/n): "));

        try {
            new UserService().create(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deletar() {
        User user = login();

        if (user == null) {
            System.out.println("Usuário não encontrado");
            return;
        }

        new UserService().delete(user);
    }

    public void listar() {
        for (User user : new UserService().fetchAll())
            printUser(user);
    }

    public void buscar() {
        int opt = openOptions("Login", "Nome");
        Collection<User> users = null;
        UserService userService = new UserService();

        switch (opt) {
        case 0:
            User user = userService.fetch(IO.readString("Login: "));

            if (user != null) {
                users = new ArrayList<User>();
                users.add(user);
            }
            break;

        case 1:
            users = userService.fetchNome(IO.readString("Nome: "));
            break;

        default:
            return;
        }

        if (users != null && users.size() > 0)
            for (User user : users)
                printUser(user);
        else
            System.out.println("Nenhum usuário encontrado");
    }

    public void pagarMulta() {
        User user = login();

        if (user == null)
            return;

        float multa = user.getMulta();

        if (multa > 0) {
            user.pagarMulta();
            System.out.println(String.format("R$%f2 pago em multas", multa));
        } else
            System.out.println("Usuário não possui multas");
    }

    public static User login() {
        String login = IO.readString("Login: ");
        User user = new UserService().fetch(login);

        if (user == null) {
            System.out.println("Usuário não encontrado, cadastre-se antes de usar o sistema");
            return null;
        }

        if (!IO.readString("Senha: ").equals(user.getSenha())) {
            System.out.println("Senha incorreta");
            return null;
        }

        return user;
    }

    private static void printUser(User user) {
        System.out.println(String.format("%s - %s (%s)", user.getLogin(), user.getNome(),
                user.getIsProfessor() ? "Professor" : "Aluno"));
    }

}