package program;

import java.sql.SQLException;

public class Program extends Menu {

    public static void main(String[] args) throws SQLException {
        new Program().open(null);
    }

    @Override
    protected String getTitulo() {
        return "Biblioteca";
    }

    @Override
    protected void draw() {
        openOptions(parent, new LivrosMenu(), new UserMenu(), new EmprestimosMenu(), new ReservaMenu());
    }

}