package program;

import java.util.Collection;

import models.Emprestimo;
import models.Reserva;
import models.User;
import services.EmprestimoService;
import services.ReservaService;

public class ReservaMenu extends Menu {

    @Override
    protected String getTitulo() {
        return "Reservas";
    }

    @Override
    protected void draw() {
        switch (openOptions("Reservar", "Cancelar reserva")) {
        case 0:
            reservar();
            break;
        case 1:
            cancelarReserva();
            break;
        default:
            parent.open(parent.parent);
            return;
        }

        open(parent);
    }

    public void reservar() {
        User user = UserMenu.login();

        if (user == null)
            return;

        Collection<Emprestimo> emprestimos = new EmprestimoService().fetchAll();

        if (emprestimos.size() == 0) {
            System.out.println("Não é possivel fazer reservas pois não há livros emprestados");
            return;
        }

        for (Emprestimo e : emprestimos)
            LivrosMenu.PrintLivro(e.getLivro());

        System.out.println("");

        int isbn = IO.readInt("ISBN: ");
        Emprestimo emprestimo = new EmprestimoService().fetch(isbn);

        if (emprestimo == null) {
            System.out.println("ISBN inválido");
            return;
        }

        Reserva reserva = Reserva.reservarLivro(emprestimo.getLivro(), user);

        if (reserva != null)
            printReserva(reserva);
        else
            System.out.println("Falha ao reservar livro");
    }

    public void cancelarReserva() {
        for (Reserva reserva : new ReservaService().fetchAll())
            printReserva(reserva);

        System.out.println("");
        int isbn = IO.readInt("ISBN: ");
        Reserva reserva = new ReservaService().fetch(isbn);

        if (reserva == null) {
            System.out.println("Livro não encontrado ou não reservado");
            return;
        }

        User user = UserMenu.login();

        if (!user.is(reserva.getUsuario())) {
            System.out.println("Usuário não autorizado a cancelar a reserva");
            return;
        }

        reserva.cancelaReserva();
    }

    private static void printReserva(Reserva reserva) {
        System.out.println(
                String.format("Livro '%s' (%d) reservado para %s (pega como %s)", reserva.getLivro().getTitulo(),
                        reserva.getIsbn(), niceDate(reserva.getRetirada()), reserva.getUsuario().getNome()));
    }

}