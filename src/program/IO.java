package program;

import java.util.Scanner;

public class IO {

    private static Scanner scanner = new Scanner(System.in);

    public static int readInt(String label, Object... args) {
        System.out.println(String.format(label, args));
        int result = scanner.nextInt();
        scanner.nextLine();
        return result;
    }

    public static String readString(String label, Object... args) {
        System.out.println(String.format(label, args));
        return scanner.nextLine();
    }

    public static boolean readBool(String label, Object... args) {
        System.out.println(String.format(label, args));
        return !scanner.nextLine().equalsIgnoreCase("n");
    }

}