package program;

import java.util.ArrayList;
import java.util.Collection;

import models.Emprestimo;
import models.Livro;
import models.User;
import services.EmprestimoService;
import services.LivrosService;
import services.ReservaService;

public class EmprestimosMenu extends Menu {

    @Override
    protected String getTitulo() {
        return "Empréstimos";
    }

    @Override
    protected void draw() {
        switch (openOptions("Efetuar empréstimo", "Renovar", "Devolver", "Listar", "Buscar")) {
        case 0:
            efetuar();
            break;
        case 1:
            renovar();
            break;
        case 2:
            devolver();
            break;
        case 3:
            listar();
            break;
        case 4:
            buscar();
            break;
        default:
            parent.open(parent.parent);
            return;
        }

        open(parent);
    }

    private void efetuar() {

        User user = UserMenu.login();

        if (user == null)
            return;

        Collection<Livro> livros = new LivrosService().fetchAll();

        if (livros.size() == 0) {
            System.out.println("Não é possivel fazer reservas pois não há livros na biblioteca");
            return;
        }

        for (Livro l : livros)
            if (new EmprestimoService().fetch(l.getISBN()) == null && new ReservaService().fetch(l.getISBN()) == null)
                LivrosMenu.PrintLivro(l);

        System.out.println("");

        int isbn = IO.readInt("ISBN: ");
        Livro livro = new LivrosService().fetch(isbn);

        if (livro == null) {
            System.out.println("Livro não encontrado");
            return;
        }

        Emprestimo e = Emprestimo.efetuarEmprestimo(livro, user);

        if (e != null)
            printEmprestimo(e);
    }

    private void renovar() {
        listar();
        int isbn = IO.readInt("ISBN: ");
        Emprestimo emprestimo = new EmprestimoService().fetch(isbn);

        if (emprestimo == null) {
            System.out.println("Livro não encontrado ou não emprestado");
            return;
        }

        emprestimo.renovar();
    }

    private void devolver() {
        listar();
        int isbn = IO.readInt("ISBN: ");
        Emprestimo emprestimo = new EmprestimoService().fetch(isbn);

        if (emprestimo == null) {
            System.out.println("Livro não encontrado ou não emprestado");
            return;
        }

        emprestimo.devolver();
    }

    private void listar() {
        for (Emprestimo e : new EmprestimoService().fetchAll())
            printEmprestimo(e);
    }

    private void buscar() {
        int opt = openOptions("ISBN", "Titulo", "Login");
        Collection<Emprestimo> emprestimos = null;
        EmprestimoService emprestimoService = new EmprestimoService();

        switch (opt) {
        case 0:
            Emprestimo user = emprestimoService.fetch(IO.readInt("ISBN: "));

            if (user != null) {
                emprestimos = new ArrayList<Emprestimo>();
                emprestimos.add(user);
            }
            break;

        case 1:
            emprestimos = emprestimoService.fetchTitulo(IO.readString("Titulo: "));
            break;

        case 2:
            emprestimos = emprestimoService.fetchLogin(IO.readString("Login: "));
            break;

        default:
            return;
        }

        if (emprestimos != null && emprestimos.size() > 0)
            for (Emprestimo emprestimo : emprestimos)
                printEmprestimo(emprestimo);
        else
            System.out.println("Nenhum empréstimo encontrado");
    }

    private void printEmprestimo(Emprestimo emprestimo) {
        if (emprestimo == null) {
            System.out.println("Empréstimo");
            return;
        }

        System.out.println(String.format(
                "Livro %s (%d) emprestado à %s, devolução até %s (atrasado há %d dias e renovado %d vezes)",
                emprestimo.getLivro().getTitulo(), emprestimo.getIsbn(), emprestimo.getUsuario().getNome(),
                niceDate(emprestimo.getEntrega()), emprestimo.getDiasAtrasados(), emprestimo.getRenovacoes()));
    }

}