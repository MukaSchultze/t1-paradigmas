package program;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import models.Livro;
import services.LivrosService;

public class LivrosMenu extends Menu {

    @Override
    protected String getTitulo() {
        return "Livros";
    }

    @Override
    protected void draw() {
        switch (openOptions("Adicionar", "Alterar", "Excluir", "Listar", "Buscar")) {
        case 0:
            Add();
            break;
        case 1:
            Update();
            break;
        case 2:
            Delete();
            break;
        case 3:
            Show();
            break;
        case 4:
            Search();
            break;
        default:
            parent.open(parent.parent);
            return;
        }

        open(parent);
    }

    private void Add() {

        int isbn = IO.readInt("ISBN: ");
        Livro livro = new LivrosService().fetch(isbn);

        if (livro != null) {
            System.out.println("Já existe um livro com este ISBN");
            return;
        }

        livro = new Livro();

        livro.setISBN(isbn);

        livro.setTitulo(IO.readString("Titulo: "));
        livro.setAno(IO.readInt("Ano de publicação: "));
        livro.setAutores(IO.readString("Autores: "));
        livro.setEditora(IO.readString("Editora: "));
        livro.setEdicao(IO.readString("Edição: "));

        try {
            new LivrosService().create(livro);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void Update() {
        Show();
        System.out.println("");

        int isbn = IO.readInt("ISBN: ");
        Livro livro = new LivrosService().fetch(isbn);

        if (livro == null) {
            System.out.println("Livro não encontrado");
            return;
        }

        livro = new Livro();

        livro.setISBN(isbn);
        livro.setTitulo(IO.readString("Titulo: "));
        livro.setAno(IO.readInt("Ano de publicação: "));
        livro.setAutores(IO.readString("Autores: "));
        livro.setEditora(IO.readString("Editora: "));
        livro.setEdicao(IO.readString("Edição: "));

        if (livro.updateOnDB())
            System.out.println("Atualizado com sucesso");
        else
            System.out.println("Falha ao atualizar");

    }

    private void Delete() {
        Show();
        System.out.println("");

        int isbn = IO.readInt("ISBN: ");
        Livro livro = new LivrosService().fetch(isbn);

        if (livro == null) {
            System.out.println("Livro não encontrado");
            return;
        }

        if (new LivrosService().delete(livro))
            System.out.println(String.format("Livro '%s' deletado", livro.getTitulo()));
        else
            System.out.println("Falha ao deletar");
    }

    public static void PrintLivro(Livro livro) {
        System.out.println(String.format("%d - %s (%d) / %s - %s - %s", livro.getISBN(), livro.getTitulo(),
                livro.getAno(), livro.getEdicao(), livro.getAutores(), livro.getEditora()));
    }

    public static void Show() {
        for (Livro livro : new LivrosService().fetchAll())
            PrintLivro(livro);
    }

    private void Search() {
        int opt = openOptions("ISBN", "Titulo", "Editora");
        Collection<Livro> livros = null;
        LivrosService livroService = new LivrosService();

        switch (opt) {
        case 0:
            Livro livro = livroService.fetch(IO.readInt("ISBN: "));

            if (livro != null) {
                livros = new ArrayList<Livro>();
                livros.add(livro);
            }
            break;

        case 1:
            livros = livroService.fetchTitulo(IO.readString("Titulo: "));
            break;

        case 2:
            livros = livroService.fetchEditora(IO.readString("Editora: "));
            break;

        default:
            return;
        }

        if (livros != null && livros.size() > 0)
            for (Livro livro : livros)
                PrintLivro(livro);
        else
            System.out.println("Nenhum livro encontrado");
    }

}