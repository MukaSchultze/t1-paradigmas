package models;

import services.UserService;

public class User extends DataSet {

    private String nome;
    private String login;
    private String senha;
    private float multa;
    private boolean isProfessor;

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return this.senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public float getMulta() {
        return this.multa;
    }

    public void setMulta(float multa) {
        this.multa = multa;
    }

    public boolean getIsProfessor() {
        return this.isProfessor;
    }

    public void setIsProfessor(boolean isProfessor) {
        this.isProfessor = isProfessor;
    }

    public boolean updateOnDB() {
        try {
            new UserService().update(this);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int limiteDiasRetirada() {
        return getIsProfessor() ? 15 : 7;
    }

    public int limiteLivrosRetirada() {
        return getIsProfessor() ? 5 : 3;
    }

    public void pagarMulta() {
        setMulta(0);
        updateOnDB();
    }

    public boolean is(User user) {
        return user.getLogin().equalsIgnoreCase(getLogin());
    }

}