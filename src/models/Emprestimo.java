package models;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import program.Menu;
import services.EmprestimoService;
import services.LivrosService;
import services.ReservaService;
import services.UserService;

public class Emprestimo extends DataSet {

    private static final float MULTA_POR_DIA = 1.00f;

    private int isbn;
    private int renovacoes;
    private String usuario;
    private LocalDateTime retirado;
    private LocalDateTime entrega;

    public int getIsbn() {
        return this.isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public Livro getLivro() {
        try {
            return new LivrosService().fetch(this.isbn);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setLivro(Livro livro) {
        this.isbn = livro.getISBN();
    }

    public User getUsuario() {
        try {
            return new UserService().fetch(this.usuario);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setUsuario(String login) {
        this.usuario = login;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario.getLogin();
    }

    public LocalDateTime getRetirado() {
        return this.retirado;
    }

    public void setRetirado(LocalDateTime retirado) {
        this.retirado = retirado;
    }

    public LocalDateTime getEntrega() {
        return this.entrega;
    }

    public void setEntrega(LocalDateTime entrega) {
        this.entrega = entrega;
    }

    public int getRenovacoes() {
        return this.renovacoes;
    }

    public void setRenovacoes(int renovacoes) {
        this.renovacoes = renovacoes;
    }

    public int getDiasAtrasados() {
        return Math.max(0, (int) ChronoUnit.DAYS.between(this.entrega, LocalDateTime.now()));
    }

    public float getMulta() {
        return getDiasAtrasados() * Emprestimo.MULTA_POR_DIA;
    }

    public static Emprestimo efetuarEmprestimo(Livro livro, User user) {

        if (livro == null) {
            System.out.println("Livro inválido");
            return null;
        }

        if (user == null) {
            System.out.println("Usuário inválido");
            return null;
        }

        if (user.getMulta() > 0) {
            System.out.println("Usuário possui multas não pagas");
            return null;
        }

        Emprestimo emprestimoAtual = new EmprestimoService().fetch(livro.getISBN());

        if (emprestimoAtual != null) {
            if (emprestimoAtual.getUsuario().is(user))
                System.out.println("Este usuário já esta com este livro");
            else
                System.out.println("Livro já emprestado para outro usuário");
            return null;
        }

        Reserva reservaAtual = new ReservaService().fetch(livro.getISBN());

        if (reservaAtual != null && !reservaAtual.getUsuario().is(user)) {
            System.out.println("Livro já está reservado para outro usuário");
            return null;
        } else if (reservaAtual != null) {
            reservaAtual.cancelaReserva();
        }

        if (new EmprestimoService().fetchLogin(user.getLogin()).size() >= user.limiteLivrosRetirada()) {
            System.out.println("Limite de livros atingido para o usuário");
            return null;
        }
        Emprestimo empr = new Emprestimo();

        empr.setUsuario(user);
        empr.setLivro(livro);
        empr.setRenovacoes(0);
        empr.setRetirado(LocalDateTime.now());
        empr.setEntrega(LocalDateTime.now().plus(user.limiteDiasRetirada(), ChronoUnit.DAYS));

        if (!empr.createOnDB()) {
            System.out.println("Falha ao criar elemento no BD");
            return null;
        }

        return empr;
    }

    public void devolver() {
        User user = getUsuario();

        user.setMulta(user.getMulta() + getMulta());
        user.updateOnDB();

        if (getMulta() > 0) {
            System.out.println("Usuário multado em R$" + getMulta());
        }

        System.out.println("Livro devolvido");

        try {
            new EmprestimoService().delete(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean renovar() {
        User user = getUsuario();

        if (user.getMulta() > 0) {
            System.out.println("Usuário possui multas não pagas");
            return false;
        }

        Reserva reservaAtual = new ReservaService().fetch(getIsbn());

        if (reservaAtual != null && !reservaAtual.getUsuario().is(user)) {
            System.out.println("Livro já está reservado para outro usuário");
            return false;
        }

        if (LocalDateTime.now().isAfter(this.entrega)) {
            System.out.println("Livro está atrasado, não é possível renovar");
            return false;
        }

        this.renovacoes++;
        this.entrega = LocalDateTime.now().plus(user.limiteDiasRetirada(), ChronoUnit.DAYS);

        if (updateOnDB()) {
            System.out.println("Livro renovado com sucesso para " + Menu.niceDate(this.entrega));
            return true;
        } else {
            System.out.println("Falha ao renovar");
            return false;
        }
    }

    public boolean createOnDB() {
        try {
            new EmprestimoService().create(this);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateOnDB() {
        try {
            new EmprestimoService().update(this);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
