package models;

import services.LivrosService;

public class Livro extends DataSet {

    private String titulo = "Desconhecido";
    private String editora = "Editora desconhecida";
    private String autores = "Autor desconhecido";
    private String edicao = "";
    private int isbn = 0;
    private int ano = 0;

    public String getTitulo() {
        return this.titulo;
    }

    public Livro setTitulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public String getEditora() {
        return this.editora;
    }

    public Livro setEditora(String editora) {
        this.editora = editora;
        return this;
    }

    public String getAutores() {
        return this.autores;
    }

    public Livro setAutores(String autores) {
        this.autores = autores;
        return this;
    }

    public String getEdicao() {
        return this.edicao;
    }

    public Livro setEdicao(String edicao) {
        this.edicao = edicao;
        return this;
    }

    public int getISBN() {
        return this.isbn;
    }

    public Livro setISBN(int isbn) {
        this.isbn = isbn;
        return this;
    }

    public int getAno() {
        return this.ano;
    }

    public Livro setAno(int ano) {
        this.ano = ano;
        return this;
    }

    public boolean updateOnDB() {
        try {
            new LivrosService().update(this);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}