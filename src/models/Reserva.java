package models;

import java.time.LocalDateTime;

import services.EmprestimoService;
import services.LivrosService;
import services.ReservaService;
import services.UserService;

public class Reserva extends DataSet {

    private int isbn;
    private String usuario;
    private LocalDateTime retirada;

    public int getIsbn() {
        return this.isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public Livro getLivro() {
        return new LivrosService().fetch(this.isbn);
    }

    public void setLivro(Livro livro) {
        this.isbn = livro.getISBN();
    }

    public User getUsuario() {
        return new UserService().fetch(this.usuario);
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setUsuario(User user) {
        this.usuario = user.getLogin();
    }

    public LocalDateTime getRetirada() {
        return this.retirada;
    }

    public void setRetirada(LocalDateTime retirada) {
        this.retirada = retirada;
    }

    public static Reserva reservarLivro(Livro livro, User user) {

        if (livro == null) {
            System.out.println("Livro inválido");
            return null;
        }

        if (user == null) {
            System.out.println("Usuário inválido");
            return null;
        }

        Emprestimo emprestimoAtual = new EmprestimoService().fetch(livro.getISBN());
        Reserva reserva = new ReservaService().fetch(livro.getISBN());

        if (emprestimoAtual == null) {
            System.out.println("O livro não pode ser reservado por não estar emprestado");
            return null;
        }

        if (reserva != null) {
            System.out.println("O livro já está reservado");
            return null;
        }

        if (user.getMulta() > 0) {
            System.out.println("Usuário possui multas não pagas e não pode reservar um livro");
            return null;
        }

        reserva = new Reserva();
        reserva.setIsbn(livro.getISBN());
        reserva.setUsuario(user);
        reserva.setRetirada(emprestimoAtual.getEntrega());

        if (!reserva.createOnDB())
            return null;

        return reserva;

    }

    public boolean cancelaReserva() {
        return new ReservaService().delete(this);
    }

    public boolean createOnDB() {
        try {
            new ReservaService().create(this);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}